# Case for the BigTreeTech TFT24 touchscreen on 2020 aluminium extrusion

![the case mounted to a frame](./img/the_case.png)

## Hardware needed:

- 7 x M3x6 hex screws
- 2 x M5x10 hex screws
- 2 x matching M5 nuts (square nuts or dedicated V- or T-slot nuts)
